#!/usr/bin/env python

import configparser

import requests
from flask import Flask, Request, request, render_template, redirect, url_for, flash
from flask_bootstrap import Bootstrap
from python_arptable import get_arp_table

from RegistrationForm import RegistrationForm

app = Flask(__name__)

config_filename = app.open_resource("config.ini").name
config = configparser.ConfigParser()
config.read(config_filename)
config = config["general"]

app.secret_key = config["secret_key"]
Bootstrap(app)


def get_mac_address(req: Request):
    mac = "Not found"

    for entry in get_arp_table():
        if entry["IP address"] == req.remote_addr:
            mac = entry["HW address"]

    return mac


@app.route('/', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    fields = [
        form.first_name,
        form.last_name,
        form.email
    ]

    if request.method == 'POST' and form.validate():
        url = config["api_url"]

        json = dict()
        json["mac_address"] = get_mac_address(request)

        for f in fields:
            json[f.name] = f.data

        headers = {
            "Authorization": "Token {}".format(config["api_token"])
        }

        response = requests.post(url, json=json, headers=headers)

        if response.status_code == 201:
            flash("Success", "success")
        else:
            msg = response.text
            if len(msg) > 100:
                msg = msg[:100] + "..."
            flash("Error {}: {}".format(response.status_code, msg), "danger")

        return redirect(url_for("register"))

    return render_template("register.html", form=form)


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
