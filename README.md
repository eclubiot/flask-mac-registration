# Registration

Simple flask application for registering wireless devices. It exposes a website where user enters his name which is then along with his device's MAC address sent to the server in the form shown below.

The application determines the MAC address using client's IP address and ARP table.

## Server request JSON
```
{
    "fullname": "Name of the registered user",
    "mac_address": "AA:BB:CC:DD:EE:FF"
}
```
