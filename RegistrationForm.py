from flask_wtf import FlaskForm
from wtforms import StringField, validators, SubmitField


class RegistrationForm(FlaskForm):
    first_name = StringField("First name", validators=[
        validators.DataRequired()
    ])
    last_name = StringField("Last name", validators=[
        validators.DataRequired()
    ])
    email = StringField("Email", validators=[
        validators.DataRequired(),
        validators.Email()
    ])
    submit = SubmitField("Sign up")
